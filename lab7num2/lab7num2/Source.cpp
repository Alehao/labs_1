#include <iostream>

using namespace std;

class my_pair {
private:
protected:
	double a, b;
public:
	bool operator==(my_pair &pair) {
		bool c = (this->a == pair.a) && (this->b == pair.b);
		return c;
	}
	my_pair operator*(my_pair &pair) {
		my_pair c;
		c.a = this->a * pair.a;
		c.b = this->b * pair.b;
		return c;
	}
	my_pair operator-(my_pair &pair) {
		my_pair c;
		c.a = this->a - pair.a;
		c.b = this->b - pair.b;
		return c;
	}

	my_pair& operator=(const my_pair &pair) {
		a = pair.a;
		b = pair.b;
		return *this;
	}

	friend ostream& operator<<(ostream& out, const my_pair &pair);

	my_pair(double a = 0, double b = 0) : a(a), b(b) {}
	my_pair(my_pair &pair) {
		a = pair.a;
		b = pair.b;
	}
	~my_pair() { /*cout << "I'm create to destroy!" << endl;*/ }
};

ostream& operator<<(ostream& out, const my_pair &pair) {
	out << '(' << pair.a << ',' << pair.b << ')';
	return out;
}


class rational : public my_pair {
private:
public:
	rational operator+(rational &pair) {
		rational c;
		c.a = this->a * pair.b + this->b * pair.a;
		c.b = this->b * pair.b;
		return c;
	}
	rational operator/(rational &pair) {
		rational c;
		c.a = this->a * pair.b;
		c.b = this->b * pair.a;
		return c;
	}
	rational& operator=(const rational &pair) {
		a = pair.a;
		b = pair.b;
		return *this;
	}
	rational operator-(rational &pair) {
		rational c;
		c.a = this->a * pair.b - this->b*pair.a;
		c.b = this->b * pair.b;
		return c;
	}
	friend ostream& operator<<(ostream& out, const rational &pair);
public:
	rational(double a = 0, double b = 0){ this->a = a; this->b = b; }
};

ostream& operator<<(ostream& out, const rational &pair) {
	out << '(' << pair.a << ',' << pair.b << ')';
	return out;
}


int main() {
	my_pair a(10, 7), b(6 , 2), l = a;
	rational c(9,11), d(5, 2);
	cout << "����� my_pair" << endl;
	cout << a - b << endl;
	cout << a * b << endl;
	if (a == b) cout << "Hello" << endl;
	if (a == l) cout << "I'm working!" << endl;
	cout << "����� rational"<<endl;
	cout << c + d << endl;
	cout << c / d << endl;
	cout << c * d << endl;
	cout << c - d << endl;

	system("pause");
	return 0;
}