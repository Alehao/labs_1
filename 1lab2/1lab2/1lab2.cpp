// 1lab2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <clocale>

using namespace std;

float max(float a, float b, float c) {
	if (a < b)
	{
		a = b;
	}
	if (a < c)
	{
		a = c;
	}
	return a;
}

//int main() {
//	float a, b, c;
//	cin >> a >> b >> c;
//	a = max(a, b, c);
//	cout << a << endl;
//	system("pause");
//	return 0;
//}

int age(int n) {
	if (n < 5) {
		if (n == 1) {
			cout << "Мне "<< n << " год";
		}
		else {
			cout << "Мне "<< n<< " года";
		}
	}
	if (n >= 5 && n <= 20) {
		cout << "Мне "<< n<< " лет";
	}
	if (n > 20) {
		if (n % 10 == 1) {
			cout << "Мне "<< n<< " год";
		}
		else if (n % 10 < 5) {
			cout << "Мне "<< n<< " года";
		}
		if (n % 10 >= 5) {
			cout << "Мне "<< n<< " лет";
		}
	}
	return 0;
}

//int main() {
//	int n;
//	setlocale(LC_CTYPE, "rus");
//	cin >> n;
//	age(n);
//	cout << endl;
//	system("pause");
//	return 0;
//}

bool touch(float x, float y,float R1,float R2) {
	bool b;
	b = (x*x + y * y <= R1 * R1 && x*x + y * y >= R2 * R2 && x*y > 0);
	return b;
}

int main() {
	setlocale(LC_CTYPE, "rus");
	float x, y,R1,R2;
	cout << "Введите радиусы" << endl;
	cin >> R1>>R2;
	if (R1 < R2) {
		x = R1;
		R1 = R2;
		R2 = x;
	}
	cout << "Введите координаты" << endl;
	cin >> x >> y;
	cout << touch(x, y, R1, R2) << endl;
	system("pause");
	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
