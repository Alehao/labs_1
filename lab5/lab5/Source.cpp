#include <iostream>
#include <time.h>
#include <clocale>
#include <stdlib.h>
#include <iomanip>

using namespace std;

int Qrand(int a, int b)
{
	return rand() % (b - a + 1) + a;
}
double Rrand(int a, int b)
{
	double k = (rand() % (b * 100 - a * 100 + 1) + a * 100);
	k /= 100;
	return k;
}
void bubble(double *a, const int n) {
	for (int j = 0; j < n; j++)
		for (int i = 0; i < n - j - 1; i++) {
			if (a[i] > a[i + 1]) {
				int t = a[i + 1];
				a[i + 1] = a[i];
				a[i] = t;
			}
		}
}
void fillQ(int **a, int n, int m);
void printQ(int **a, int n, int m);
void fillR(double **a, int n, int m);
void fillRQ(double **a, int n, int m);
void printR(double **a, int n, int m);
int lessThenMax(int **a, int n, int m);
void spiral(int **a, int n);
void rowSort(double **a, int n, int m);
void countSome(double **a, int n, int m);
void multiply(int **a, int na, int ma, int **b, int nb, int mb);
void createB(int **a, int **b, int n, int m);
int eqution(double **a, int n, double *dotX, double *dotY);
int countDot(double *dotX, double *dotY, int n);

int main()
{
	//setlocale(LC_ALL, "Russian");
	srand((unsigned)time(0));

	// 1
	/*int n, m;
	cin >> n >> m;
	int **a = new int *[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[m];
	fillQ(a, n, m);
	printQ(a, n, m);*/

	// 2
	/*int n, m;
	cin >> n >> m;
	double **a = new double *[n];
	for (int i = 0; i < n; i++)
		a[i] = new double[m];
	fillR(a, n, m);
	printR(a, n, m);*/

	// 3
	/*int n = 7, m = 5;
	int **a = new int *[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[m];
	fillQ(a, n, m);
	printQ(a, n, m);
	cout << lessThenMax(a, n, m);*/

	// 4
	/*int n;
	cin >> n;
	int **a = new int *[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[n];
	fillQ(a, n, n);
	printQ(a, n, n);
	spiral(a, n);*/

	// 5 
	/*int n, m;
	cin >> n >> m;
	double **a = new double *[n];
	for (int i = 0; i < n; i++)
		a[i] = new double[m];
	fillR(a, n, m);
	printR(a, n, m);
	rowSort(a, n, m);
	printR(a, n, m);*/

	// 6 
	/*int n, m;
	cin >> n >> m;
	double **a = new double *[n];
	for (int i = 0; i < n; i++)
		a[i] = new double[m];
	fillR(a, n, m);
	printR(a, n, m);
	countSome(a, n, m);*/

	// 7 
	/*int n, ma, nb, mb;
	do {
		cout << "������� na, ma, nb = ma, mb" << endl;
		cin >> n >> ma >> nb >> mb;
	} while (ma != nb);
	int **a = new int *[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[ma];
	int **b = new int *[nb];
	for (int i = 0; i < nb; i++)
		b[i] = new int[mb];
	fillQ(a, n, ma);
	fillQ(b, nb, mb);
	printQ(a, n, ma);
	printQ(b, nb, mb);
	multiply(a, n, ma, b, nb, mb);
	
	for (int i = 0; i < nb; i++)
		delete[] b[i];
	delete[] b;*/
	
	// 9 
	//double dotX[5000],dotY[5000];
	//int n = Qrand(1, 100);

	//cin >> n;
	//if (n > 1) {
	//	double **eq = new double *[n];
	//	for (int i = 0; i < 2; i++)
	//		eq[i] = new double[n];

	//	if (n > 6) {
	//		fillRQ(eq, 2, n);
	//		printR(eq, 2, n);
	//	}
	//	else {
	//		for (int i = 0; i < n; i++) {
	//			cin >> eq[0][i] >> eq[1][i];
	//		}
	//	}

	//	n = eqution(eq, n, dotX, dotY);
	//	//bubble(dotX, n);
	//	cout << countDot(dotX, dotY, n);

	//	for (int i = 0; i < 2; i++)
	//		delete[] eq[i];
	//	delete[] eq;
	//}
	//else cout << "0";


	// 67
	int n=10, m=10;
	int **a = new int *[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[m];
	fillQ(a, n, m);
	printQ(a, n, m); 

	int **b = new int *[n];
	for (int i = 0; i < n; i++)
		b[i] = new int[m];

	createB(a, b, 10, 10);

	printQ(b, n, m);
	for (int i = 0; i < n; i++)
		delete[] b[i];
	delete[] b;


	for (int i = 0; i < n; i++)
		delete[] a[i];
	delete[] a;
	system("pause");
	return 0;
}
void fillRQ(double **a, int n, int m) {
	for (int j = 0; j < n; j++)
		for (int i = 0; i < m; i++)
			a[j][i] = Qrand(-100, 100);
}
void fillQ(int **a, int n, int m)
{
	for (int j = 0; j< n;j++)
		for (int i = 0; i < m; i++)
			a[j][i] = Qrand(-100, 100);
}
void printQ(int **a, int n, int m)
{
	for (int j = 0; j < n; j++) {
		for (int i = 0; i < m; i++)
			cout << setfill(' ')<< setw(10) << a[j][i];
		cout << endl;
	}
	cout << endl;
}
void fillR(double **a, int n, int m)
{
	for (int j = 0; j < n; j++)
		for (int i = 0; i < m; i++)
			a[j][i] = Rrand(-100, 100);
}
void printR(double **a, int n, int m)
{
	for (int j = 0; j < n; j++) {
		for (int i = 0; i < m; i++)
			cout << setfill(' ') << setw(8) << a[j][i];
		cout << endl;
	}
	cout << endl;
}

int lessThenMax(int **a, int n, int m)
{
	int max = 0, count = 0;
	for (int j = 0; j < n; j++)
		for (int i = 0; i < m; i++)
			if (a[j][i] > max) {
				max = a[j][i];
				count = 1;
				continue;
			}
			else if (a[j][i] == max) count++;
	return n * m - count;
}

void spiral(int **a, int n)
{
	int top = n / 2, bot = top, left = top, right = top;
	int j = top, i = top;
	cout << a[j][i] << " ";
	while (true) {
		if (j <= n / 2) {
			while (j <= top) {
				if (j == n-1) break;
				j++;
				cout << a[j][i] << " ";
			}
			top = j;
		}
		if (j == n - 1 && i == 0) break;
		if (i <= n / 2) {
			while (i <= right) {
				i++;
				cout << a[j][i] << " ";
			}
			right = i;
		}
		if (j >= n / 2) {
			while (j >= bot) {
				j--;
				cout << a[j][i] << " ";
			}
			bot = j;
		}
		if (i >= n / 2) {
			while (i >= left) {
				i--;
				cout << a[j][i] << " ";
			}
			left = i;
		}
	}
}

void rowSort(double **a, int n, int m) 
{
	double *sum = new double [n];
	for (int j = 0; j < n; j++) {
		sum[j] = 0;
		for (int i = 1; i < m; i += 2) {
			sum[j] += a[j][i];
		}
	}
	for (int j = 0; j < n; j++)
		for (int i = 0; i < n - j - 1; i++) {
			if (sum[i] < sum[i + 1]) {
				double r = sum[i];
				sum[i] = sum[i + 1];
				sum[i + 1] = r;
				for (int k = 0; k < m; k++) {
					double t = a[i][k];
					a[i][k] = a[i + 1][k];
					a[i + 1][k] = t;
				}
			}
		}
	delete []sum;
}

void countSome(double **a, int n, int m)
{
	int **count = new int *[3];
	for (int i = 0; i < 3; i++)
		count[i] = new int[m];

	for (int j = 0; j< 3;j++)
		for (int i = 0; i < m; i++) {
			count[j][i] = 0;
		}

	for (int j = 0; j < n; j++)
		for (int i = 0; i < m; i++) {
			if (a[j][i] < 0) count[0][i] ++;
			else if (!a[j][i]) count[1][i] ++;
			else if (a[j][i] > 0) count[2][i] ++;
		}

	for (int j = 0; j < 3; j++) {
		for (int i = 0; i < m; i++) {
			cout << setfill(' ') << setw(8) << count[j][i];
		}
		switch (j) {
			case 0:cout << setfill(' ') << setw(8) << "x<0"; break;
			case 1:cout << setfill(' ') << setw(8) << "x=0"; break;
			case 2:cout << setfill(' ') << setw(8) << "x>0"; break;
		}
		
		cout << endl;
	}
	
	for (int i = 0; i < 3; i++)
		delete[] count[i];
	delete[] count;
 }

void multiply(int **a, int na, int ma, int **b, int nb, int mb)
{
	int **res = new int *[na];
	for (int i = 0; i < na; i++)
		res[i] = new int[mb];

	for (int j = 0; j < na; j++)
		for (int i = 0; i < mb; i++) {
			res[j][i] = 0;
			for (int t = 0; t < ma; t++)
				res[j][i] += a[j][t] * b[t][i];
		}
	
	printQ(res, na, mb);

	for (int i = 0; i < na; i++)
		delete[] res[i];
	delete[] res;
}

void createB(int **a, int **b, int n, int m) 
{
	int max, t, k;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			t = i; k = j; max = -101; ;
			while (t < n) {
				while (k - t < j - i && k < m - 1) {
					if (a[t][k] > max) max = a[t][k];
					k++;
				}
				if (a[t][k] > max) max = a[t][k];
				t++;
				k = i + j - t;
				if (k < 0) k = 0;
			}
			b[i][j] = max;
		}
	}
}

int eqution(double **a, int n, double *dotX, double *dotY)
{
	int t = 0;
	for (int i = 0; i < n - 1; i++) {
		for (int j = i + 1; j < n; j++) {
			if (a[0][i] != a[0][j]) {
				dotX[t] = (a[1][j] - a[1][i]) / (a[0][i] - a[0][j]);
				dotY[t] = (a[1][i] * a[0][j] - a[1][j] * a[0][i]) / (a[0][j] - a[0][i]);
				t++;
			}
		}
	}
	return t;
}

int countDot(double *dotX, double *dotY, int n)
{
	int res = 0;
	//cout << dotX[0];//
	//for (int i = 0; i < n; i++) cout << dotX[i] << " ";//
	//cout << endl;//
	//for (int i = 0; i < n; i++) cout << dotY[i] << " ";//
	//cout << endl;//
	for (int i = 0; i < n - 1; i++) {
		for (int j = i + 1; j < n; j++) {
			if (abs(dotX[i] - dotX[j]) < 0.0000001 && abs(dotY[i] - dotY[j]) < 0.0000001) {
				//cout << dotX[i] << "   " << dotX[j]<<endl;//
				//cout << dotY[i] << "   " << dotY[j]<<endl;//
				res++;
				break;
			}
		}
	}
	//cout << endl;//
	//cout << n << endl << res << endl;//
	return n - res;
	/*int res = 0, i = 0;
	while (i < n-1) {
		for (int j = i + 1; j < n - 1; j++) {

		}
		i++;
	}*/

}

//int countDot(double *dotX, double *dotY, int n)
//{
//	int res = 0;
//	cout << dotX[0];//
//	for (int i = 1; i < n; i++) {
//		if (dotX[i] == dotX[i - 1]) res++;
//		cout << dotX[i] << " ";//
//	}
//	cout << endl;//
//	return n - res;
//}

//int main()
//{
//	srand((unsigned)time(0));
//	double dot[5000];
//	int n = Qrand(1, 100);
//
//	cin >> n;
//	if (n > 1) {
//		double **eq = new double *[n];
//		for (int i = 0; i < 2; i++)
//			eq[i] = new double[n];
//
//		if (n > 6) {
//			fillR(eq, 2, n);
//			printR(eq, 2, n);
//		}
//		else {
//			for (int i = 0; i < n; i++) {
//				cin >> eq[0][i] >> eq[1][i];
//			}
//		}
//
//		n = eqution(eq, n, dot);
//		buble(dot, n);
//		cout << countDot(dot, n);
//
//		for (int i = 0; i < 2; i++)
//			delete[] eq[i];
//		delete[] eq;
//	}
//	else cout << "0";
//
//	system("pause");
//	return 0;
//}