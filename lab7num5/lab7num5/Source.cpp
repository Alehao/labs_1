#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <windows.h>
#include <ctime>
#include <string>
#include <iomanip>

using namespace std;


struct subject {
	int numSub, semester, fullHours, classroomHours, lecturs, practice;
	int typeOfSub, typeOfCheck;

	string aboutSub;
	int homeWorkHours;
	bool courseWork;

	enum typeOfSub {
		federal,
		regional,
		choice
	};
	enum typeOfCheck{
		test,
		exam
	};
	char name[81];

	void setNumSub(int numSub) { this->numSub = numSub; }
	void setSemester(int semester) { this -> semester = semester; }
	void setFullHours(int fullHours) { this->fullHours = fullHours; }
	void setClassroomHours(int classroomHours) { this->classroomHours = classroomHours; }
	void setLecturs(int lecturs) { this->lecturs = lecturs; }
	void setPractice(int practive) { this->practice = practice; }
	void setCourseWork(bool courseWork) { this->courseWork = courseWork; }
	void setAboutDisp(string aboutSub) { this->aboutSub = aboutSub; }

	int getNumSub() { return this->numSub; }
	int getSemester() { return this->semester; }
	int getFullHours() { return this->fullHours; }
	int getClassroomHours() {return this->classroomHours; }
	int getLecturs() { return this->lecturs; }
	int getPractice() { return this->practice; }

	subject(const char *name, int semester, int fullHours, int classroomHours, int lecturs, int typeOfCheck, int typeOfSub, bool courseWork = 0, string s = "") {
		strcpy(this->name, name);
		this->semester = semester;
		this->fullHours=fullHours;
		this->classroomHours = classroomHours;
		this->lecturs = lecturs;
		practice = classroomHours - lecturs;
		this->typeOfCheck = typeOfCheck;
		this-> typeOfSub = typeOfSub;
		this->aboutSub = s;
		this->courseWork = courseWork;
	}
	subject() {}
	friend istream& operator>>(istream &in, subject &sub);
	friend ostream& operator<<(ostream &out, const subject &sub);
};

ostream& operator<<(ostream &out, const subject &sub) {
	const int n = 30;
	out.setf(ios::left);
	out << setfill(' ') << setw(n) << "�������� ����������" << sub.name << endl;
	out << setfill(' ') << setw(n) << "������� ����������" << sub.semester << endl;
	out << setfill(' ') << setw(n) << "����� ����� �� ��������" << sub.fullHours << endl;
	out << setfill(' ') << setw(n) << "���������� ����" << sub.classroomHours << endl;
	out << setfill(' ') << setw(n) << "���������� ����" << sub.lecturs << endl;
	out << setfill(' ') << setw(n) << "���� ��������" << sub.practice << endl;
	out << setfill(' ') << setw(n) << "������� �������� ������";
	if (sub.courseWork) cout << "����" << endl;
	else cout << "���" << endl;
	if (sub.typeOfCheck == subject::typeOfCheck::exam)
		out << setfill(' ') << setw(n) << "�������� �������� � ����� ��������" << endl;
	else
		out << setfill(' ') << setw(n) << "�������� �������� � ����� ������" << endl;
	switch (sub.typeOfSub) {
		case subject::typeOfSub::federal:
			out << setfill(' ') << setw(n) << "����������� ����������" << endl; break;
		case subject::typeOfSub::regional:
			out << setfill(' ') << setw(n) << "������������ ����������" << endl; break;
		case subject::typeOfSub::choice:
			out << setfill(' ') << setw(n) << "���������� �� ������" << endl; break;
	}
	return out;
}
istream& operator>>(istream &in, subject &sub) {
	cout << "�������� ����������" << endl;
	in.getline(sub.name, 81);
	while (*sub.name == NULL)
		in.getline(sub.name, 81);
	cout << "� ����� ������� �������� ����������" << endl;
	in >> sub.semester;
	cout << "����� ��������� ����� �� �������� ����������" << endl;
	in >> sub.fullHours;
	cout << "���������� ���������� �����" << endl;
	in >> sub.classroomHours;
	cout << "���������� ����� ������" << endl;
	in >> sub.lecturs;
	cout << "���������� ����� �������" << endl;
	in >> sub.practice;
	cout << "��� ����������"<< endl<< "0 - �����������, 1 - ������������, 2 - �� ������" << endl;
	in >> sub.typeOfSub;
	cout << "��� ��������� ��������" << endl << "0 - �����, 1 - �������" << endl;
	in >> sub.typeOfCheck;
	return in;
}

class PlanEducation {
private:
	int codeSpecialty;
	char *nameSpecialty;
	int day, month, year;
	int hoursStd;
	subject subjList[30];
	int numSubj;

	int countedHours[6], numExams[6], numTests[6];
public:
	void addSubject(subject &a) {
		subjList[numSubj] = a;
		subjList[numSubj].numSub = numSubj;
		numSubj++;
	}
	void deleteSubject(char *s) {
		for (int i = findName(s); i < numSubj - 1; i++) {
			subjList[i + 1].numSub = i;
			subjList[i] = subjList[i + 1];
		}
		numSubj--;
	}
	void deleteSubject(subject &a) {
		for (int i = a.numSub; i < numSubj - 1; i++) {
			subjList[i + 1].numSub = i;
			subjList[i] = subjList[i + 1];
		}
		numSubj--;
	}
	void deleteSubject(int number) {
		for (int i = number; i < numSubj - 1; i++) {
			subjList[i + 1].numSub = i;
			subjList[i] = subjList[i + 1];
		}
		numSubj--;
	}

	int findName(char *s) {
		for (int i = 0; i < numSubj; i++) {
			if (!strcmp(subjList[i].name, s)) {
				return i;
			}
		}
		return -1;
	};
	int findSemester(int semester) {
		for (int i = 0; i < numSubj; i++) {
			if (subjList[i].semester == semester) {
				return i;
			}
		}
		return -1;
	};
	int findTypeOfSubj(enum typeOfSub a) {
		for (int i = 0; i < numSubj; i++) {
			if (subjList[i].typeOfSub == a) {
				return i;
			}
		}
		return -1;
	};
	int findTypeOfCheck(enum typeOfCheck a) {
		for (int i = 0; i < numSubj; i++) {
			if (subjList[i].typeOfCheck == a) {
				return i;
			}
		}
		return -1;
	};

	void countHours() {
		//countedHours[6] = { 0,0,0,0,0,0 };
		for (int i = 0; i < 6; i++)
			countedHours[i] = 0;
		for (int i = 0; i < numSubj; i++) 
			countedHours[subjList[i].semester - 1] += subjList[i].fullHours;
		for (int i = 0; i < 2; i++) {
			if (countedHours[i] > 1000) {
				cout << nameSpecialty << endl;
				cout << "����� " << "� �������� " << i + 1 << " ������ 1000" << endl;
			}
			if (countedHours[i] < 300) {
				cout << nameSpecialty << endl;
				cout << "����� " << "� �������� " << i + 1 << " ������ 300" << endl;
			}
		}
	};
	void countExamsAndTests() {
		for (int i = 0; i < 6; i++) {
			numExams[i] = 0;
			numTests[i] = 0;
		}
		for (int i = 0; i < numSubj; i++) {
			if (subjList[i].typeOfCheck == subject::typeOfCheck::exam) {
				numExams[subjList[i].semester - 1]++;
				
			}
			else {
				numTests[subjList[i].semester - 1]++;
				
			}
		}
		for (int i = 0; i < 2; i++) {
			if (numExams[i] > 6) {
				cout << nameSpecialty << endl;
				cout << "��������� " << "� �������� " << i + 1 << " ������ 5" << endl;
			}
			if (numExams[i] < 2) {
				cout << nameSpecialty << endl;
				cout << "��������� " << "� �������� " << i + 1 << " ������ 2" << endl;
			}
		}
	}

	int getExams(int semester) { return numExams[semester - 1]; }
	int getTests(int semester) { return numTests[semester - 1]; }
	int getCountedHours(int semester) { return countedHours[semester - 1]; }

	PlanEducation(): numSubj(0) {}
	PlanEducation(const char *nameSpecialty, int hours, int codeSpecialty, 
		int day, int month, int year, subject *subjList, int numSubj) {
		this->nameSpecialty = const_cast<char*>(nameSpecialty);
		this->hoursStd = hours;
		this->codeSpecialty = codeSpecialty;
		this->day = day;
		this->month = month;
		this->year = year;
		for (int i = 0; i < numSubj; i++) {
			addSubject(subjList[i]);
			this->subjList[i].numSub = i;
		}
		this->numSubj = numSubj;
		countHours();
		countExamsAndTests();
	}
	/*friend ostream& operator<<(ostream &out, const Payment &Pay);
	friend istream& operator>>(istream &in, Payment &Pay);   ������� �� ���������� �����  */ 

	friend ostream& operator<<(ostream &out, const PlanEducation &a);
	friend struct group;

	PlanEducation operator+(PlanEducation &a) {
		PlanEducation c = *this;
		for (int i = 0; i < a.numSubj; i++)
			if (!(c.findName(a.subjList[i].name) + 1))
				c.addSubject(a.subjList[i]);
		return c;
	}
	PlanEducation operator-(PlanEducation &a) {
		PlanEducation c = *this;
		for (int i = 0; i < c.numSubj; i++)
			if (a.findName(c.subjList[i].name)+1)
				c.deleteSubject(a.subjList[i]);
		return c;
	}
	PlanEducation operator&(PlanEducation &a) {
		PlanEducation c = *this;
		for (int i = 0; i < c.numSubj; i++)
			if (!(a.findName(c.subjList[i].name) + 1))
				c.deleteSubject(a.subjList[i]);
		return c;
	}
};
ostream& operator<<(ostream &out, const PlanEducation &a) {
	out << "���:\t" << a.codeSpecialty << endl;
	out << "��������:\t" << a.nameSpecialty << endl;
	out << "���� ��������:\t" << a.day <<'.'<< a.month<<'.'<< a.year << endl;
	out << "���� �� �������� �� ���������:\t" << a.hoursStd << endl;
	out << "����� ���������:\t" << a.numSubj << endl;

	
	for (int i = 0; i < 6; i++) {
		out << "����� � �������� " << i + 1 << ":\t\t" << a.countedHours[i] << endl;
		out << "��������� � �������� " << i + 1 << ":\t\t" << a.numExams[i] << endl;
		out << "������� � �������� " << i + 1 << ":\t\t" << a.numTests[i] << endl;
	}
	return out;
}

struct group {
private:
	char *name;
	int *num, n, choose[30];
	subject *sub;
public:
	group(PlanEducation &a) {
		name = a.nameSpecialty;
		sub = a.subjList;
		num = &(a.numSubj);
		n = *num;
		for (int i = 0; i < n; i++)
			choose[i] = i;
	}
	group() {}
	void getGroup(PlanEducation &a) {
		name = (a.nameSpecialty);
		sub = (a.subjList);
		num = (&(a.numSubj));
		n = *num;
		for (int i = 0; i < n; i++)
			choose[i] = i;
	}
	void getSemester(int semester) {
		n = 0;
		for (int i = 0; i < *num; i++) {
			if (sub[i].semester == semester) {
				choose[n] = i;
				n++;
			}
		}
	}
	void getType(int typeOfSub) {
		n = 0;
		for (int i = 0; i < *num; i++) {
			if (sub[i].typeOfSub == typeOfSub) {
				choose[n] = i;
				n++;
			}
		}
	}
	void reset() {
		n = *num;
		for (int i = 0; i < n; i++)
			choose[i] = i;
	}
	void getTypeOfCheck(int typeOfCheck) {
		n = 0;
		for (int i = 0; i < *num; i++) {
			if (sub[i].typeOfCheck == typeOfCheck) {
				choose[n] = i;
				n++;
			}
		}
	}
	void getCourseWork() {
		n = 0;
		for (int i = 0; i < *num; i++) {
			if (sub[i].courseWork) {
				choose[n] = i;
				n++;
			}
		}
	}
	friend ostream& operator<<(ostream &out, const group &a) {
		for (int i = 0; i < a.n; i++)
			out << a.sub[a.choose[i]];
		return out;
	}
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	//time_t t;
	//cout << time(&t);
	subject sub1("����� � ������ ����������������", 1, 158, 108, 50, subject::typeOfCheck::exam, subject::typeOfSub::federal, 1),
		sub2("�������������� ������", 1, 200, 108, 60, subject::typeOfCheck::exam, subject::typeOfSub::regional),
		sub3("�������", 1, 158, 108, 50, subject::typeOfCheck::test, subject::typeOfSub::choice, 1),
		sub4("�������������� ������ ����������������", 2, 158, 108, 50, subject::typeOfCheck::test, subject::typeOfSub::regional),
		sub5("���������", 2, 109, 108, 50, subject::typeOfCheck::exam, subject::typeOfSub::choice),
		sub6("������� � ���������", 2, 300, 108, 50, subject::typeOfCheck::exam, subject::typeOfSub::regional, 1),
		sub7("���������� ����������", 2, 200, 108, 50, subject::typeOfCheck::exam, subject::typeOfSub::federal),
		plan1[30], plan2[30], plan3[30];
	plan1[0] = sub1; plan1[1] = sub2; plan1[2] = sub4; plan1[3] = sub5; plan1[4] = sub6;
	plan2[0] = sub1; plan2[1] = sub3; plan2[2] = sub4; plan2[3] = sub5; plan2[4] = sub7;
	plan3[0] = sub1; plan3[1] = sub2; plan3[2] = sub5; plan3[3] = sub6; plan3[4] = sub7;
	PlanEducation first("���������� ���������� � ����������", 400, 351812, 30, 11, 1999, plan1, 5),
		second("����� �����������", 400, 351842, 30, 11, 2003, plan2, 5),
		third("�������� ����������", 400, 361820, 21, 5, 2002, plan3, 5), c;
	cout << first;
	group firstG(first), cG;
	/*cout << endl << firstG;
	firstG.getSemester(1);
	cout << endl << firstG;*/
	cout << endl <<  "������ ���������" << endl;


	c = first - second;
	cG.getGroup(c);
	cout << endl << cG;

	c = first & second;
	cG.reset();
	cout << endl << cG;

	c = first + second;
	cG.reset();
	cout << endl << cG;


	cG.getSemester(1);
	cout << endl << endl<< "����� �� ��������" << endl << endl<< cG;
	cG.getCourseWork();
	cout << endl << endl << "����� �� ��������" << endl << endl << cG;
	cG.getType(1);
	cout << endl << endl << "����� ������������ ���������" << endl << endl << cG;
	cG.getTypeOfCheck(1);
	cout << endl << endl << "����� ��������� � ����������" << endl << endl << cG;

	//int b = const_cast <int> (a);
	system("pause");
	return 0;
}