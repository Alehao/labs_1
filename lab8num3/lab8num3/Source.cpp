#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <fstream>

using namespace std;

int randNum() { return rand() % 101 - 50; }

void fillFile(string way) {
	ofstream outf(way);
	generate_n(ostream_iterator<int>(outf, " "), 100, randNum);
	outf.close();
}

vector<int> fillVec(string way) {
	ifstream inf(way);
	vector<int> a(100);
	for_each(a.begin(), a.end(), [&](int &i) {inf >> i; });
	inf.close();
	return a;
}

struct swap_struct {
	int max;
	swap_struct() {}
	void operator()(int &i) { if (i > 0) i = max; }
} swap_max;


vector <int> swapMax(vector<int> a) {
	swap_max.max = *max_element(a.begin(), a.end());
	for_each(a.begin(),a.end(), swap_max);
	return a;
}

void out(int i) {
	cout << setfill(' ')<<setw(5)<< i;
}
int main() {
	srand((unsigned)time(0));
	vector <int> vec1(100), vec2;
	string way = "C:\\Users\\Alex\\source\\repos\\lab8num3\\input.txt";
	fillFile(way);
	vec1 = fillVec(way);
	for_each(vec1.begin(), vec1.end(), out);

	cout << endl;
	vec2 = swapMax(vec1);
	for_each(vec2.begin(), vec2.end(), out);
	ofstream outf("C:\\Users\\Alex\\source\\repos\\lab8num3\\output.txt");
	for_each(vec2.begin(), vec2.end(), [&](int i) { outf << i << endl; });
	outf.close();
	system("pause");
	return 0;
}