#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <iostream>
#include <ctime> 
#include <iomanip>
using namespace std;

//class BitString {
//	unsigned long a;
//	unsigned long b;
//};

class Payment {
private:

	char fullName[81];
	int salary, percent, tax, finPayment;
	int dayWorkedMonth, workingDayMonth, hiringYear, exp;
	int accruedSum, withheldSum;
	SYSTEMTIME time;

	void countAccrued() { //�����������
		accruedSum = salary * dayWorkedMonth / workingDayMonth;
		countPayment();
		countWithheld();
	}
	void countWithheld() { //����������
		withheldSum = accruedSum * tax / 100;
	}
	void countPayment() { //���������� �� ����
		finPayment = accruedSum * (100 - tax) / 100;
	}
	void countExp() { //����
		exp = time.wYear - hiringYear;
	}
	void recount() {
		countAccrued();
		countExp();
	}
public:
	bool operator>(Payment &Pay) { return this->salary > Pay.salary; }
	bool operator<(Payment &Pay) { return this->salary < Pay.salary; }
	bool operator>=(Payment &Pay) { return this->salary >= Pay.salary; }
	bool operator<=(Payment &Pay) { return this->salary <= Pay.salary; }
	bool operator==(Payment &Pay) { return this->salary == Pay.salary; }
	bool operator!=(Payment &Pay) { return this->salary != Pay.salary; }
	
	Payment(char *fullName = NULL) {
		if (fullName != NULL)
			strcpy(this->fullName, fullName);
		else
			strcpy(this->fullName, "");
		GetSystemTime(&time);
		tax = 14;
		percent = 10;
		salary = 20000;
		hiringYear = time.wYear;
		dayWorkedMonth = 21;
		workingDayMonth = 21;
		countAccrued();
		countWithheld();
		countExp();
		countPayment();
	}
	Payment(char *fullName, int tax, int percent, int salary, int hiringYear,
		int dayWorkedMonth, int workingDayMonth) :
		salary(salary), tax(tax), percent(percent), hiringYear(hiringYear),
		dayWorkedMonth(dayWorkedMonth), workingDayMonth(workingDayMonth)
	{
		strcpy(this->fullName, fullName);
		GetSystemTime(&time);
		countAccrued();
		countWithheld();
		countExp();
		countPayment();
	}

	Payment(const Payment &A) {
		GetSystemTime(&time);
		strcpy(this->fullName, A.fullName);
		tax = A.tax;
		percent = A.percent;
		salary = A.salary;
		hiringYear = A.hiringYear;
		dayWorkedMonth = A.dayWorkedMonth;
		workingDayMonth = A.workingDayMonth;
		countAccrued();
		countWithheld();
		countExp();
		countPayment();
	}
	~Payment() {
		//cout << "� ���������� :3" << endl;
		//delete[] fullName;
	}
	void setName(char *n);
	void setSalary(int n);
	void setHiringYear(int n);
	void setPercent(int n);
	void setTax(int n);
	void setDayWorkedMonth(int n);
	void setWorkingDayMonth(int n);
	void setAccruedSum(int n);
	void setWitheldSum(int n);

	char *getName();
	int getSalary();
	int getHiringYear();
	int getPercent();
	int getTax();
	int getDayWorkedMonth();
	int getWorkingDayMonth();
	int getAccruedSum();
	int getWitheldSum();
	int getExp();
	int getPayment();

	friend ostream& operator<<(ostream &out, const Payment &Pay);
	friend istream& operator>>(istream &in, Payment &Pay);
};

ostream& operator<<(ostream &out, const Payment &Pay) {
	const int n = 30;
	out.setf(ios::left);
	out << setfill(' ') << setw(n) << "������ ���:"				<< Pay.fullName << endl;
	out << setfill(' ') << setw(n) << "�����:"					<< Pay.salary << endl;
	out << setfill(' ') << setw(n) << "��� �����:"				<< Pay.hiringYear << endl;
	out << setfill(' ') << setw(n) << "����:"					<< Pay.exp << endl;
	out << setfill(' ') << setw(n) << "������� ��������:"		<< Pay.percent << endl;
	out << setfill(' ') << setw(n) << "������� ������:"			<< Pay.tax << endl;
	out << setfill(' ') << setw(n) << "����������� ����:"		<< Pay.dayWorkedMonth << endl;
	out << setfill(' ') << setw(n) << "������� ���� � ������:"	<< Pay.workingDayMonth << endl;
	out << setfill(' ') << setw(n) << "C���� ��� ��������:"		<< Pay.accruedSum << endl;
	out << setfill(' ') << setw(n) << "���������� �����:"		<< Pay.withheldSum << endl;
	out << setfill(' ') << setw(n) << "����������� �����:"		<< Pay.finPayment << endl;
	return out;
}
istream& operator>>(istream &in, Payment &Pay) {
	const int n = 30;
	cout.setf(ios::left);
	cout << setfill(' ') << setw(n) << "������� ������ ���:";
	in.getline(Pay.fullName, 81);
	while (!strcmp(Pay.fullName, ""))
		in.getline(Pay.fullName, 81);
	cout << setfill(' ') << setw(n) << "������� �����:";
	in >> Pay.salary;
	cout << setfill(' ') << setw(n) << "������� ��� �����:";
	in >> Pay.hiringYear;
	cout << setfill(' ') << setw(n) << "������� ������� ��������:";
	in >> Pay.percent;
	cout << setfill(' ') << setw(n) << "������� ������� ������:";
	in >> Pay.tax;
	cout << setfill(' ') << setw(n) << "������� ����������� ����:";
	in >> Pay.dayWorkedMonth;
	cout << setfill(' ') << setw(n) << "������� ������� ���� � ������:";
	in >> Pay.workingDayMonth;
	Pay.recount();
	return in;
}

//class Longlong

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	char s1[] = "�������� ���� ����������", s2[] = "������ ���� ��������";
	Payment p(s1), l(s2, 14, 18, 30000, 2000, 18, 21), u = p, c;

	cout << p << endl << l << endl << c << endl;
	cin >> c;
	cout << endl << c << endl;
	cin >> p;
	cout << endl << p;

	cout << endl;
	
	cout << (p > l);
	cout << (p < l);
	cout << (p >= l);
	cout << (p <= l);
	cout << (p == l);
	cout << (p != l);

	system("pause");
	return 0;
}

void Payment::setName(char *n) {
	strcpy(this->fullName, n);
}
void Payment::setSalary(int n) {
	this->salary = n;
	this->countAccrued();
}
void Payment::setHiringYear(int n) {
	this->hiringYear = n;
	this->countExp();
}
void Payment::setPercent(int n) {
	this->percent = n;
	this->countAccrued();
}
void Payment::setTax(int n) {
	this->tax = n;
	this->countAccrued();
}
void Payment::setDayWorkedMonth(int n) {
	this->dayWorkedMonth = n;
	this->countAccrued();
}
void Payment::setWorkingDayMonth(int n) {
	this->workingDayMonth = n;
	this->countAccrued();
}
void Payment::setAccruedSum(int n) {
	this->accruedSum = n;
	this->countAccrued();
}
void Payment::setWitheldSum(int n) {
	this->withheldSum = n;
}

char *Payment::getName() {
	return this->fullName;
}
int Payment::getSalary() {
	return this->salary;
}
int Payment::getHiringYear() {
	return this->hiringYear;
}
int Payment::getPercent() {
	return this->percent;
}
int Payment::getTax() {
	return this->tax;
}
int Payment::getDayWorkedMonth() {
	return this->dayWorkedMonth;
}
int Payment::getWorkingDayMonth() {
	return this->workingDayMonth;
}
int Payment::getAccruedSum() {
	return this->accruedSum;
}
int Payment::getWitheldSum() {
	return this->withheldSum;
}
int Payment::getExp() {
	return this->exp;
}
int Payment::getPayment() {
	return this->finPayment;
}