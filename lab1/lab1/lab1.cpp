// lab1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	float z1f, z2f,m;
	double z1d, z2d,n;
	cin >> n;
	cin >> m;
	z1f = sqrt((3 * m + 2)*(3 * m + 2) - 24 * m) / abs(3 * sqrt(m) - 2 / sqrt(m));
	z2f = sqrt(m);
	z1d = sqrt((3 * n + 2)*(3 * n + 2) - 24 * n) / abs(3 * sqrt(n) - 2 / sqrt(n));
	z2d = sqrt(n);
	cout << setprecision(30);
    cout << z1f << endl; 
	cout << z2f << endl;
	cout << z1d << endl;
	cout << z2d << endl;
	cout << "z1f - z2f = " << abs(z1f - z2f) << endl;
	cout << "z1d - z2d = " << abs(z1d - z2d) << endl;
	cout << "z1f - z1d = " << abs(z1f - z1d) << endl;
	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
