#include <iostream>
#include <time.h>
#include <clocale>
#include <stdlib.h>
#include <iomanip>

using namespace std;

int Qrand(int a, int b)
{
	return rand() % (b - a + 1) + a;
}
double Rrand(int a, int b)
{
	double k = (rand() % (b * 100 - a * 100 + 1) + a * 100);
	k /= 100;
	return k;
}
void fillA(int *a, const int n);
void printA(int *a, const int n);
void fillAR(double *a, const int n);
void printAR(double *a, const int n);
double sumR(double *a, const int n);
long long mult(int *a, const int n);
int maxQ(int *a, const int n);
void change(double *a, const int n, double C);
int find(double *a, const int n, double C, double eps);
void sortR(double *a, const int n);
void pressR(double *a, const int n, double min, double max);
void fastSort(int *a, const int s, const int n);
int binSearch(int *a, const int n, int k);
void numS(int n);
int binom(int m, int n);
void test(int *a, const int n);


void buble(int *a, const int n) {
	for (int j = 0; j < n; j++)
		for (int i = 0; i < n - j - 1; i++) {
			if (a[i] > a[i + 1]) {
				int t = a[i + 1];
				a[i + 1] = a[i];
				a[i] = t;
			}
		}
}

int main() {
	setlocale(LC_ALL, "Russian");
	srand((unsigned)time(0));
	// 1
	/*int n;
	cin >> n;
	int *a = new int[n];
	fillA(a, n);
	printA(a, n);
	delete(a);*/

	// 2
	/*int n;
	cin >> n;
	double *aR = new double[n];
	fillAR(aR, n);
	printAR(aR, n);
	delete(aR);*/

	// 3 
	/*int n;
	cin >> n;
	double *aR = new double[n];
	fillAR(aR, n);
	printAR(aR, n);
	cout << sumR(aR, n);
	delete(aR);*/

	// 4
	/*int n;
	cin >> n;
	int *a = new int[n];
	fillA(a, n);
	printA(a, n);
	cout << mult(a, n);
	delete(a);*/

	//5
	/*int n;
	cin >> n;
	double *aR = new double[n];
	fillAR(aR, n);
	printAR(aR, n);
	double C;
	cin >> C;
	change(aR, n, C);
	printAR(aR, n);
	delete(aR);*/

	// 6
	/*int n;
	cin >> n;
	double *aR = new double[n];
	fillAR(aR, n);
	printAR(aR, n);
	double C;
	double eps;
	cin >> C;
	cin >> eps;
	cout << find(aR, n, C, eps);
	delete(aR);*/

	// 7
	/*int n;
	cin >> n;
	double *aR = new double[n];
	fillAR(aR, n);
	printAR(aR, n);
	sortR(aR, n);
	cout << endl;
	printAR(aR, n);
	delete(aR);*/

	// 8
	/*int n;
	cin >> n;
	double *aR = new double[n];
	fillAR(aR, n);
	printAR(aR, n);
	cout << endl;
	double m, b;
	cin >> m >> b;
	pressR(aR,n,m,b);
	printAR(aR, n);
	delete(aR);*/

	// 9 ,10 �������
	int n, k;
	cin >> n;
	int *a = new int[n];
	fillA(a, n);
	printA(a, n);
	fastSort(a, 0, n-1);
	printA(a, n);
	//test(a, n);
	cin >> k;
	cout << binSearch(a,n,k);
	delete []a;

	// 133
	/*int n;
	cin >> n;
	numS(n);*/

	// 151
	/*int n, m;
	cin >> m >> n;
	cout << binom(m, n);*/

	system("pause");
	return 0;
}
void fillA(int *a, const int n) {
	for (int i = 0; i < n; i++) a[i] = Qrand(-100, 100);
}
void printA(int *a, const int n) {
	for (int i = 0; i < n; i++) cout << i << setfill(' ') << setw(10) << a[i] << endl;
	cout << endl;
}
void fillAR(double *a, const int n) {
	for (int i = 0; i < n; i++) a[i] = Rrand(-100, 100);
}
void printAR(double *a, const int n) {
	for (int i = 0; i < n; i++) cout << i << setfill(' ') << setw(10) << a[i] << endl;
}
double sumR(double *a, const int n) {
	double s = 0;
	for (int i = 0; i < n; i += 2) s += a[i];
	return s;
}
long long mult(int *a, const int n)
{
	long long res = 1;
	for (int i = maxQ(a, n) + 1; i < n; i++)
		res *= a[i];
	return res;
}
int maxQ(int *a, const int n)
{
	int max_i = 0;
	for (int i = 0; i < n; i++)
		if (a[i] > a[max_i]) max_i = i;
	return max_i;
}
void change(double *a, const int n, double C)
{
	for (int i = 0; i < n; i++)
		if (a[i] > C) a[i] = 0;
}
int find(double *a, const int n, double C, double eps)
{
	for (int i = 0; i < n; i++)
		if (abs(a[i] - C) < eps) return i;
	return -1;
}
void sortR(double *a, const int n)
{
	for (int i = 0; i < n; i++) {
		int min = i;
		for (int j = i + 1; j < n; j++)
			if (a[j] < a[min]) min = j;
		double k = a[i];
		a[i] = a[min];
		a[min] = k;
	}
}
void pressR(double *a, const int n, double min, double max)
{
	int count = 0;
	if (min > max) {
		double t = min;
		min = max;
		max = t;
	}
	for (int i = 0; i < n; i++) {
		if (count) {
			a[i - count] = a[i];
			a[i] = 0;
		}

		if (a[i - count] >= min && a[i - count] <= max) {
			a[i - count] = 0;
			count++;
		}
	}
}
void fastSort(int *a, const int s, const int n)
{
	int l = s, r = n, m = a[(n + s) / 2];
	while (l < r) {
	if (a[l] < m) l++;
	if (a[r] >= m) r--;
	if (a[l] >= m && a[r] <= m && l<=r) {
		int t = a[l];
		a[l] = a[r];
		a[r] = t;
		l++; r--;
		}
	}
	if (s < r) fastSort(a, s, r);
	if (l < n) fastSort(a, l, n);

}
int binSearch(int *a, const int n, int k)
{
	int left = 0, right = n - 1;
	if (a[left] == k) return left;
	if (a[right] == k) return right;
	while (left < right - 1) {
		int mid = (right + left + 1) / 2;
		if (a[mid] == k) return mid;
		if (a[mid] < k) 
			left = mid;
		if (a[mid] > k)
			right = mid;
	}
	return -1;
}
void numS(int n)
{
		long long an, bn, cn, dn;
		for (int a = 1; a <= 1000; a++)
			for (int b = a; b <= 1000; b++)
				for (int c = 1; c <= 1000; c++)
					for (int d = c; d <= 1000; d++) {
						if (a == c && b == d || a == d && b == c) continue;
						an = 1; bn = 1; cn = 1; dn = 1;
						for (int i = 0; i < n; i++) {
							an *= a; bn *= b; cn *= c; dn *= d;
						}
						if (an + bn == cn + dn) {
							cout << an + bn << endl;
							cout << an << " " << bn << " " << cn << " " << dn << " " << endl;
							cout << a << " " << b << " " << c << " " << d << " " << endl;
							return;
						}
					}
		return;
}
int binom(int m, int n)
{
	if (m == 0 && n > 0 || m == n && n >= 0) return 1;
	if (m > n && n >= 0) return 0;
	/*return binom(m, n - 1)*n / (n - m);*/
	return binom(m - 1, n - 1) + binom(m, n - 1);
}
void test(int *a, const int n)
{
	for (int j = 0; j < 100; j++) {
		for (int i = 0; i < n - 1; i++) {
			if (a[i] > a[i + 1]) {
				cout << "0";
				return;
			}
		}
		fillA(a, n);
		fastSort(a, 0, n - 1);
		printA(a, n);
	}
	cout << "1";
}