#include <iostream>
#include <cmath>
#include <algorithm>
#include <string>
#include <stack>
#include <map>
#include <sstream>
#include <exception>
#include <Windows.h>

using namespace std;

double calc(char sym, double a, double b = 0) {
	switch (sym) {
	case '+': return a + b;
	case '-': return a - b;
	case '*': return a * b;
	case '/': 
		if (b == 0) throw exception("������� �� ����");
		return a / b;
	case '^': 
		if (a == 0 && b == 0) throw exception("���� � ������� ����");
		return pow(a, b);
	case 's': return sin(a);
	case 'a': 
		if (a < -1 || a > 1) throw exception("�������� �� ����������� ������� �������� ���������");
		return asin(a);
	case 'n': 
		if (a) return -a;
		else return a;
	}
}
// ������� length!
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	stack<double> nums;
	stack<char> syms;
	map<char, short> prior;
	prior.insert(pair<char, short>('+', 1));
	prior.insert(pair<char, short>('-', 1));
	prior.insert(pair<char, short>('*', 2));
	prior.insert(pair<char, short>('/', 2));
	prior.insert(pair<char, short>('^', 3));
	prior.insert(pair<char, short>('s', 5));
	prior.insert(pair<char, short>('a', 5));
	prior.insert(pair<char, short>('n', 5));
	//prior.insert(pair<char, short>('n', 5));
	char sym, numsAr[] = "1234567890";
	double *num = NULL, forNumPointer = 0;
	int dotCount = 0, openNum = 0, length = 0;
	bool countFlag = 0;
	string s = "(32)=";
	istringstream in(s);
	in >> sym;
	length++;
	try {
		while (sym != '=') {
			//���� sym �����
			if (find(numsAr, numsAr + 11, sym) != numsAr + 11 || sym =='.') {
				if (num == NULL)
					num = &forNumPointer;
				if (countFlag)
					dotCount++;
				if (sym != '.')
					*num = *num * 10 + sym - '0';
				if (sym == '.' && !countFlag)
					//if (countFlag) throw exception("���� ����, �� �����?");
					countFlag = 1;
			}
			//���� sym �� �����
			else {
				//
				if (num == NULL && sym !='(' && s[length-2]!=')') {
					syms.pop();
					syms.push(sym);
				}
				else {
					if (num != NULL) {
						dotCount = pow(10, dotCount);
						nums.push(*num / dotCount);
						dotCount = 0;
						countFlag = 0;
						*num = 0;
						num = NULL;
					}
					else 
						if ( sym != '(' && s[length-2]!=')')
							nums.empty()?nums.push(0):nums.push(nums.top());
					if (syms.empty() || prior[syms.top()] < prior[sym] || sym == '(') {
						if (prior[sym] == 5) {
							double a = nums.top();
							nums.pop();
							a = calc(sym, a);
							nums.push(a);
						}
						else {
							syms.push(sym);
							if (sym == '(') openNum++;
						}
					}
					else if (prior[syms.top()] >= prior[sym] || sym == ')') {
						while (!syms.empty() && syms.top() != '(' && prior[syms.top()] >= prior[sym]) {
							double a = nums.top();
							nums.pop();
							a = calc(syms.top(), nums.top(), a);
							nums.pop();
							syms.pop();
							nums.push(a);
						}
						if (!syms.empty() && syms.top() == '(' && sym == ')') {
							syms.pop();
							openNum--;
						}
						if (sym != ')')
							syms.push(sym);
					}
				}
			}
			in >> sym;
			length++;
		}
		if (num != NULL) {
			dotCount = pow(10, dotCount);
			nums.push(*num / dotCount);
			dotCount = 0;
			countFlag = 0;
			*num = 0;
			num = NULL;
		}
		else {
			nums.push(nums.top());
		}
		while (syms.size() != 0) {
			double a = nums.top();
			nums.pop();
			if (syms.top() == '(') syms.pop();
			a = calc(syms.top(), nums.top(), a);
			nums.pop();
			syms.pop();
			nums.push(a);
		}
	cout << nums.top();
	}
	catch (exception exc) {
		cout << exc.what() << endl;
	}
	system("pause");
	return 0;
}




//#include <iostream>
//#include <cmath>
//#include <algorithm>
//#include <string>
//#include <stack>
//#include <map>
//#include <sstream>
//#include <exception>
//#include <Windows.h>
//
//using namespace std;
//
//double calc(char sym, double a, double b = 0) {
//	switch (sym) {
//	case '+': return a + b;
//	case '-': return a - b;
//	case '*': return a * b;
//	case '/':
//		if (b == 0) throw exception("������� �� ����");
//		return a / b;
//	case '^':
//		if (a == 0 && b == 0) throw exception("���� � ������� ����");
//		return pow(a, b);
//	case 's': return sin(a);
//	case 'a':
//		if (a < -1 || a > 1) throw exception("�������� �� ����������� ������� �������� ���������");
//		return asin(a);
//	case 'n':
//		if (a) return -a;
//		else return a;
//	}
//}

//int main() {
//	SetConsoleCP(1251);
//	SetConsoleOutputCP(1251);
//	stack<double> nums;
//	stack<char> syms;
//	map<char, short> prior;
//	prior.insert(pair<char, short>('+', 1));
//	prior.insert(pair<char, short>('-', 1));
//	prior.insert(pair<char, short>('*', 2));
//	prior.insert(pair<char, short>('/', 2));
//	prior.insert(pair<char, short>('^', 3));
//	prior.insert(pair<char, short>('s', 5));
//	prior.insert(pair<char, short>('a', 5));
//	prior.insert(pair<char, short>('n', 5));
//	//prior.insert(pair<char, short>('n', 5));
//	char sym, numsAr[] = "1234567890";
//	double *num = NULL, forNumPointer = 0;
//	int dotCount = 0, openNum = 0, length = 0;
//	bool countFlag = 0;
//	string s = "2+-2.1n=";
//	istringstream in(s);
//	in >> sym;
//	length++;
//	try {
//		while (sym != '=') {
//			if (find(numsAr, numsAr + 11, sym) != numsAr + 11 || sym == '.') {
//				if (num == NULL)
//					num = &forNumPointer;
//				if (countFlag)
//					dotCount++;
//				if (sym != '.')
//					*num = *num * 10 + sym - '0';
//				if (sym == '.')
//					countFlag = 1;
//			}
//			else {
//				if (length >= 2 && prior.find(s[length - 2]) != prior.end()) {
//					syms.pop();
//					syms.push(sym);
//				}
//				else {
//					if (num != NULL) {
//						dotCount = pow(10, dotCount);
//						nums.push(*num / dotCount);
//						dotCount = 0;
//						*num = 0;
//						num = NULL;
//					}
//					else if (sym == '-')
//						nums.push(0);
//					if (syms.empty() || prior[syms.top()] < prior[sym] || sym == '(') {
//						if (prior[sym] == 5) {
//							double a = nums.top();
//							nums.pop();
//							a = calc(sym, a);
//							nums.push(a);
//						}
//						else {
//							syms.push(sym);
//							if (sym == '(') openNum++;
//						}
//					}
//					else if (prior[syms.top()] >= prior[sym] || sym == ')') {
//						while (!syms.empty() && syms.top() != '(' && prior[syms.top()] >= prior[sym]) {
//							double a = nums.top();
//							nums.pop();
//							a = calc(syms.top(), nums.top(), a);
//							nums.pop();
//							syms.pop();
//							nums.push(a);
//						}
//						if (!syms.empty() && syms.top() == '(' && sym == ')') {
//							syms.pop();
//							openNum--;
//						}
//						if (sym != ')')
//							syms.push(sym);
//					}
//				}
//			}
//			in >> sym;
//			length++;
//		}
//		if (num != NULL) {
//			dotCount = pow(10, dotCount);
//			nums.push(*num / dotCount);
//			dotCount = 0;
//			*num = 0;
//			num = NULL;
//		}
//		while (syms.size() != 0) {
//			double a = nums.top();
//			nums.pop();
//			a = calc(syms.top(), nums.top(), a);
//			nums.pop();
//			syms.pop();
//			nums.push(a);
//		}
//		cout << nums.top();
//	}
//	catch (exception exc) {
//		cout << exc.what() << endl;
//	}
//	system("pause");
//	return 0;
//}