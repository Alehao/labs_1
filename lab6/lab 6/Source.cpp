#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cstring>
#include <windows.h>
#include <fstream>

//typedef char dictionary[2];

using namespace std;

struct complex {
	double Re, Im;
	bool init(double a, double b) {
		if (abs(a) > 0 || abs(b) > 0)
			return true;
		return false;
	}
	void enter() {
		double a, b;
		do {
			cout << "������� �������������� ����� �����:" << endl;
			cin >> a;
			cout << "������� ����������� ����� �����:" << endl;
			cin >> b;
	} while (!init(a, b));
		Re = a;
		Im = b;
	}
	void print() {
		if (Im > 0)
			cout << Re << "+" << Im << "i";
		else if (Im == 0)
			cout << Re;
		else
			cout << Re << Im << "i";
	}
	
};

complex operator / (complex A, complex B) {
	complex res;
	res.Re = (A.Re*B.Re + A.Re*B.Im)/(B.Im*B.Im + B.Re*B.Re);
	res.Im = (B.Re*A.Im - A.Re*B.Im)/(B.Im*B.Im + B.Re*B.Re);
	return res;
}

void getShort(char *fullFIO, char *shortFIO);
char toupRus(int c);
char tolowRus(int c);
bool checkPalindrom(char *str);
void preparing(char *rowText);
bool search(char dictionary[][81], char *word, int numOfWords);
void readDictionary(char dict[][81], int &n);
void sortDictionary(char dict[][81], int n);
void writeDictionary(char dict[][81], int n);
void writeCapitalLetter();
void writeSentence();
complex division(complex A, complex B);

int main() {
	SetConsoleCP(1251); 
	SetConsoleOutputCP(1251);

	//������� 1 �����
	/*char fullFIO[80], shortFIO[80];
	cin.getline(fullFIO, 80);
	getShort(fullFIO, shortFIO);
	cout << shortFIO;*/
	//������ ���� ��������

	//������� 2 �����
	/*for (int i = '�'; i <= '�';i++)
		cout << toupRus(i);
	cout << endl;
	for (int i = '�'; i <= '�';i++)
		cout << tolowRus(i);*/
	
	//������� 3
	/*char str[81];
	cin.getline(str, 80);
	cout << checkPalindrom(str);*/
	//� ���� ����� �� ���� �����

	//������� 4
	/*char link[81] = "C:\\Users\\Alex\\source\\repos\\lab6\\Text\\Text.txt";
	preparing(link);
	char dict[1000][81];
	int n = 0;
	readDictionary(dict, n);
	sortDictionary(dict, n);
	writeDictionary(dict, n);*/

	//������� 5
	/*char link[81] = "C:\\Users\\Alex\\source\\repos\\lab6\\Text\\test1.txt";
	preparing(link);
	writeCapitalLetter();*/

	//������� 6
	//char link[81] = "C:\\Users\\Alex\\source\\repos\\lab6\\Text\\test2.txt";
	////preparing(link);
	//writeSentence();

	//������� 7
	/*complex a, b, c;
	a.enter();
	b.enter();
	c = division(a, b);
	c.print();
	a = a / b;
	a.print();*/

	system("pause");
	return 0;
}

void getShort(char *fullFIO, char *shortFIO) {
	char *str;
	str = strtok(fullFIO, " ");
	strcpy(shortFIO, str);
	strcat(shortFIO, " ");
	for (int i = 0; i < 2; i++) {
		str = strtok(NULL, " ");
		strncat(shortFIO, str, 1);
		strcat(shortFIO, ".");
	}
}

char toupRus(int c) {
	if (c >= '�'&&c <= '�') {
		c += '�'-'�';
	}
	return c;
}

char tolowRus(int c) {
	if (c >= '�'&&c <= '�') {
		c -= '�' - '�';
	}
	return c;
}

bool checkPalindrom(char *str) {
	char str2[81], def[] = " ,.-;:()!?";
	char *pnt = strtok(str, def);
	strcpy(str2, pnt);
	pnt = strtok(NULL, def);
	while (pnt != NULL) {
		strcat(str2, pnt);
		pnt = strtok(NULL, def);
	}
	strcpy(str, " ");
	int len = strlen(str2) - 1;
	for (int i = len; i >= 0; i--) {
		str2[i] = tolowRus(str2[i]);
		str[len - i] = str2[i];
	}
	str[len + 1] = '\0';
	return !strcmp(str, str2);
}

void preparing(char *rowText) {
	char finText[] = "C:\\Users\\Alex\\source\\repos\\lab6\\Text\\Input.txt";
	ifstream in(rowText);
	ofstream out(finText);
	char s[81] = "\0",sout[81] = "\0";
	if (!in) {
		cout << "�� �����(";
		return;
	}
	if (!out) {
		cout << "�� �����(";
		return;
	}
	while (!in.eof()) {
		in.getline(s, 81, ' ');
		if (strcspn(s, "\n") != strlen(s) && strcspn(s, "\n") != 0) {
			int t = in.tellg();
			in.seekg(t - strlen(s) - 3);
			in.getline(s, 81);
		}
		if (strlen(sout) + 1 + strlen(s) > 80 || strcspn(s, "\n") == 0) {
			out << sout << endl;
			strcpy(sout, s);
			sout[strlen(s)] = ' ';
			sout[strlen(s)+1] = '\0';
		}
		else {
			if (*sout != NULL)
				strcat(sout, " ");
			strcat(sout, s);
		}
	}
	out << sout << endl;
	in.close();
	out.close();
}

bool search(char dictionary[][81], char *word, int numOfWords) {
	for (int i = numOfWords; i >= 0; i--) {
		if (!strcmp(word, dictionary[i])) {
			return 0;
		}
	}
	return 1;
}

void readDictionary(char dict[][81], int &n) {
	char s[81], del[] = " .,-[]()1234567890\":!?";
	ifstream in("C:\\Users\\Alex\\source\\repos\\lab6\\Text\\Input.txt");
	if (!in) {
		cout << "�� �����(";
		return ;
	}
	char *pnt;
	while (!in.eof()) {
		in.getline(s, 81);
		pnt = strtok(s, del);
		while (pnt != NULL) {
			for (int i = 0; i < strlen(pnt); i++) {
				pnt[i] = tolowRus(pnt[i]);
			}
			if (search(dict, pnt, n)) {
				strcpy(dict[n], pnt);
				n++;
			}
			pnt = strtok(NULL, del);
		}
	}
	in.close();
}

void sortDictionary(char dict[][81], int n) {
	for (int i = 0; i < n; i++)
		for (int j = 1; j < n - i; j++)
			if (strcmp(dict[j - 1], dict[j]) >= 0) {
				char s[81];
				strcpy(s, dict[j - 1]);
				strcpy(dict[j - 1], dict[j]);
				strcpy(dict[j], s);
			}
}

void writeDictionary(char dict[][81], int n) {
	ofstream out("C:\\Users\\Alex\\source\\repos\\lab6\\Text\\Output.txt");
	if (!out) {
		cout << "�� �����(";
		return;
	}
	for (int i = 0; i < n; i++)
		out << dict[i] << endl;
	out.close();
}

void writeCapitalLetter() {
	char s[81], del[] = " .,-[]()1234567890\":!?";
	ifstream in("C:\\Users\\Alex\\source\\repos\\lab6\\Text\\Input.txt");
	if (!in) {
		cout << "�� �����(";
		return;
	} 
	while (!in.eof()) {
		in.getline(s, 81);
		char *pnt = strtok(s, del);
		while (pnt != NULL) {
			if (pnt[0] >= '�' && pnt[0] <= '�')
				cout << pnt << endl;
			pnt = strtok(NULL, del);
		}
	}
	in.close();
}

void writeSentence() {
	char dot, s[81];
	int start, fin, dpos, cpos, n = 0, numPos;
	bool flag = false;
	ifstream in("C:\\Users\\Alex\\source\\repos\\lab6\\Text\\Input.txt");
	if (!in) {
		cout << "�� �����(";
		return;
	}
	start = in.tellg();
	while (!in.eof()) {

		in.getline(s, 81);
		n++;
		
		numPos = strcspn(s, "1234567890");
		if (numPos != strlen(s))
			if (s[numPos + 1] >= '0' && s[numPos + 1] <= '9' && (s[numPos + 2] < '0' || s[numPos + 2] > '9'))
				flag = true;

		
		dpos = strcspn(s, ".!?");
		cpos = in.tellg();
		
		switch (s[dpos]) 
		{
			case '.': dot = '.'; break;
			case '!': dot = '!'; break;
			case '?': dot = '?'; break;
			default: continue;
		}

		fin = cpos - strlen(s) + dpos;
		if (flag) {
			in.seekg(start);
			for (int i = 0; i < n - 1; i++) {
				in.getline(s, 81);
				cout << s <<" ";
			}
			in.getline(s, 81, dot);
			cout << s <<dot;
			flag = false;
		}
		start = fin - 1;
		in.seekg(start);
		n = 0;
	}
	cout << endl;
	in.close();
}

complex division(complex A, complex B) {
	complex res;
	res.Re = (A.Re*B.Re + A.Re*B.Im) / (B.Im*B.Im + B.Re*B.Re);
	res.Im = (B.Re*A.Im - A.Re*B.Im) / (B.Im*B.Im + B.Re*B.Re);
	return res;
}