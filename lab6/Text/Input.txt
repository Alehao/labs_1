����, ������ ��� �������� � ��������� �������� �������, ���������� �����������
���������������  � 1859 ����. ����� ��������� "� ������� ������������ ��������".
����������  ������� ����� ������� � ������������ ������ ����������� ��������,
�������  ��� ����� � ����� ��������� ��� ������.

����������  ����� ������� ���������, ����������� ����, ������������ � ������
�����  ����� ����. � ������ ��� �� ������ � ��������� ������� ��������� �
�������  ������������. ����� ��������� ��������. ������ ������, ������� ��� ����
����  ��������, �������� ����� ���������� ��������, ��������� ��� ��������
�������  ������������, � ��������, ���������, ������������ ������������� ���,
����  ������� �������� �����. ���� ����� ����������, �������, ����������
�������������  �������� ������ ��������� � �����, ����� ������ �������. ������
��������,  �������� � ������� "� ������� ������������ ��������", ������ �
�����������  � ������ ����� ���������� ��������� ����� ��������� �� ������� ����
������. 

������  ������ ������, � ��� ������ ����������� ��� ������ �����. � � ������
������  ���������� ��������� ������������ ��������� 43 ������ �����, � �
�����������  ���� �� �������, ������� ��������� � ���� ������ ������. ���
��������  �������� ������� ���������� ��������� � �������� ���������, �� �
������  ��� ����������, ��������� ��� ��������.[1] ����� ���������, ����������
����  ����������� ��� � �������� �����, ����� ���������������� � ������. �, ���
��  �����, �� ������������ ������ ������� ���������� �� � ������� ����� ��� 2
000  ���, ����� ��� ���, � ������ �������, ��� ������, �� ������� ����
��������������,  ������ ������� ����� �������������� � ������� ����. ������ ���?
������  ��� �������� ���� ����� �������, ��� �������� ����. � ���� �� ���
�������  ������������� ���� ������ ������������ �� �����������, �� �����������
����������.  �� � ������ ������ �������� ���� ����������. �� �������� �����
��������  �����, ��� ����� ��������� ������, ���� ����� ������������� ��������
�����������  ��������. ��� �������������� ������ �� ��������� ������
��������������  ������ �������. � ��� ������������� ������, �� ������ ������
����,  � ������ ����� ����, ��������, �������������.

��  ����������� ������� � ����� ���������, ��� ����� �� ���������� ����������
���  ���������. �, ����������, ���� � ���� ���������, ������� ������ ���������
����-������  ������ �, �������������, ������ �������� ��������������.

�����  ��� ��������� �������� ������� ���, ��� ��� ����������� � ��������
����������  ����� � �������� ����������� ����������� �� ���������, ��� ��, ����
���  ��������, ���������� ����������� ��� ��������, �������������� ��� ��������
�  ������ ����. ��������� ����� ������������ � ��������� ������ ��������
�����������������  ������ ������������ � ��������������� ��� ���������
������������  � ������. ������������ ������� ����� ������� ������������ ��������
��  ��� ��� ������. � ���� �������, ������ ��� ������ ������� ������������ ���
����  ������������� �������. �� ���� �������� �������� ������ ����������
��������  ������� �� ������ �������, � ������� ���������� ����������
������������  � �������������������� �������, ��� �������� ��������������
�����������  ���� ���, ��� � �������� ���� ������� ������ �� ��� �����, �� �
������  ���� �������� ���: De te fabula narratur! [He ���� �� ������� ���!].

����  �����, ���� �� ����, �� � ����� ��� ����� ������� ������� �������� ���
������������  ������������, ������� �������� �� ������������ �������
������������������  ������������. ���� � ����� ���� �������, � ���� ����������,
�����������  � ���������������� � �������� ��������������.

������,  ����������� ����� ��������, ���������� ����� �������� ������ ����
�������  �� ������������ ��������.

��  ����� ����. ���, ��� � ��� ������ ������������ �����������������
������������,  ��������, �� �������� � ����������� ������, ���� ������� �������
����  ����������, ��� ��� �� �� ����� ����������� � ���� ��������� �������. ��
����  ��������� �������� ��, ��� � ������ ��������������� ������ ��������

������,  �������� �� ������ �� �������� ������������������ ������������, �� �
��  ���������� ��� ��������. ������ � ���������� ����������� ����� ��� ������
�����  ��� �������������� ��������, ������������ ���������� ����, ��� ����������
���������  �����������, �������� ���� ������� ������������ � ������������� ��
���������  ������������ � ������������ ���������. �� �������� �� ������ ��
�����,  �� � �� �������. Le mort saisit le vif! [������� ������� ������!]

��  ��������� � ����������, ���������� ���������� �������� � ���������
���������������  ����� �������� ������ ��������� � ������ ���������. ������ ���
������������  ��������� ��� ��� ���������, ����� ����������� ��� ��� ������
������.  ��������� ����� ����������� ��� �������� �� ���, ���� �� ����
�������������  � ���������� ��������� ������������, ��� ��� �������� � ������,
��������  �� ������������ ������������� �������, ���� �� ��� �������� ����
��������  ������ �� ������������ ��� ��������� ������, ��� � ������, ���� ��
�������  ����� ��� ���� ���� ����� �� ������������, ��������������� �
�����������  �����, ��� ���������� ��������� ����������, ���������� �����,
������������  ������ � "Public Health" ("�������� ���������"), ��� �����
����������  ��������, ������������� ������� ������������ ������ � �����,
���������  �����, ������� � �. �. ������ �������� � �����-���������, �����
������������  �������. �� ��������� ������-���������� ����� � ���, ����� �����
�����������  �������� ����� ������������� �������.

������  ����������� ��������. ������� ���� ��� ������������ ����� XVIII
��������  �� ������������� ���������� �������� ��������� ��� �����������
���������,  ��� �� ��������� � �������� ������ ������ �� �� ���� �������
�����������  ����� � ������� XIX ��������. � ������ ������� ���������� ���� ���
������  ������������. ��������� ��������� �������, �� ������ ������������ ��
���������.  �� ������ ����� ����� �������� ��� ����� �������� ����� �
�����������  �� ������ �������� ������ �������� ������. ����� �������, ������
�����-����  ������� ����� �������� �������, ����������� ������� ��������������
����  ������� ������������ ������ ��� �� ���������� �������� �������� ������
�����������,  ������� ��������� ���������������� �������������. ������-��, �����
������,  � ������ � ��������� ���� ����� ������������ ����� �������, ����������
�  ����������� ����������� ���������� ����������������. ������ ����� ����� �
������  ������� � ������. ��������, ���� ���� ��� ������ �� ���� �������������
������  ������ ��������,� � �������� ����� ����� ��������� �������� ��������
��������������  ������ �������� ������������ ��������, � �� ����� �� �����������
�����  ������������ ���� ��������, �� �������� ��������� ���������. �� ��� �����
���������  � �������� ���� �����.
