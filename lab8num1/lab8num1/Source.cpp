#include <iostream>
#include <windows.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

string met(char *st) {
	ifstream in(st);
	if (!in) {
		cout << "alarm";
	}
	//ofstream out("C:\\Users\\Alex\\source\\repos\\lab8num1\\textout.txt");
	ostringstream out;
	string s;
	char s1[100];
	size_t begin;
	size_t end;
	while (!in.eof()) {
		getline(in, s);
		begin = s.find("begin");
		if (begin != std::string::npos) {
			s.erase(begin, 5);
			s.insert(begin, "{");
		}
		end = s.find("end");
		if (end != std::string::npos) {
			s.erase(end, 3);
			s.insert(end, "}");
		}
		out << s << endl;
	}

	in.close();
	return out.str();
}

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	char s[81] = "C:\\Users\\Alex\\source\\repos\\lab8num1\\textin.txt";
	string out;
	out = met(s);
	cout << out;
	system("pause");
	return 0;
}