#include <iostream>
#include <iomanip>
#include <clocale>
#include <stdlib.h>
#include <time.h>
using namespace std;

float math(int n);
void input();
void game();
void NOD(int a,int b);
double func(double x, double eps);
int irand(int a,int b);
void func1(double xS, double xF, double dx, double a, double b, double c);
int FibNum(int n);
void ln(double xS, double xF, double dx, double eps);
double kadA(double x, double y, double z);
double kadB(double x, double y, double z);


int main() 
{
	setlocale(LC_CTYPE, "rus");
	srand((unsigned)time(0));
	//Num 1
	/*int n;
	cin >> n;
	cout << math(n);*/

	//Num 2
	//input();

	//Num 3
	//game();

	//Num 4
	/*int a, b;
	cin >> a >> b;
	NOD(a,b);*/

	//Num 5
	/*double x, eps;
	cin >> x >> eps;
	cout << func(x, eps);*/

	//Num 6 ??
	/*double xS, xF, dx, a, b, c;
	cin >> xS >> xF >> dx >> a >> b >> c;
	func1(xS, xF, dx, a, b, c);*/

	//Num 7 ���������
	double xS, xF, dx, eps;
	cin >> xS >> xF >> dx >> eps;
	ln(xS,xF,dx,eps);

	//Num 8 �� �������
	/*int n;
	cin >> n;
	cout << FibNum(n);*/

	//Num 9 �� �������
	/*double x, y, z;
	cin >> x >> y >> z;
	cout << kadA(x, y, z) << endl;
	cout << kadB(x, y, z) << endl;*/

	system("pause");
	return 0;
}

float math(int n)
{
	float a = 0;
	for (int i = 1; i <= n; i++) {
		a += 1 / ((float)i * ((float)i + 1));
	}
	return a;
}
void input()
{
	int a;
	do {
		cout << "������� ����� �� 10 �� 58" << endl;
		cin >> a;
	} while (a<10 || a>58);
	cout << a;
}
void game()
{

	int a, b;
	b = irand(10, 58);
	cout << "������� �����" << endl;
	cin >> a;
	while (a!=b) {
		if (a > b) {
			cout << "������!";
		}
		else {
			cout << "������!";
		}
		cout << "������� �����" << endl;
		cin >> a;
	}
	cout << "���������!";
}
void NOD(int a, int b)
{
	while (a != b) {
		if (a > b) {
			a -= b; 
		}
		else
			 b -= a;
	}
	cout << a;
}
double func(double x, double eps)
{
	double y1=1, y2=0;
	while (abs(y1-y2)>=eps) {
		y2 = y1;
		y1 = (y2 + x / y2) / 2;
	}
	return y1;
}

void func1(double xS, double xF, double dx, double a, double b, double c)
{
	int k = ((int)a | (int)b) & !((int)a | (int)c);
	double F;
	cout << "|" << setw(10) << setfill(' ') << "X" << "|";
	cout << setw(10) << setfill(' ') << "F " << "|" << endl;
	while (xS <= xF) {
		if (xS < -10 && b)
			F = a * xS*xS - c * xS + b;
		else if (xS > -10 && !b)
			F = (xS - a) / (xS - c);
		else
			F = -xS / (a - c);
		if (k) {
			cout << "|" << setw(10) << setfill(' ') << xS << "|";
			cout << setw(10) << setfill(' ') << F << "|" << endl;
		}
		else {
			cout << "|" << setw(10) << setfill(' ') << xS << "|";
			cout << setw(10) << setfill(' ') << (int)F << "|" << endl;
		}
		xS += dx;
	}
	if (xS != xF) {
		if (xF < -10 && b)
			F = a * xF*xF - c * xF + b;
		else if (xF > -10 && !b)
			F = (xF - a) / (xF - c);
		else
			F = -xF / (a - c);
	}
	if (k) {
		cout << "|" << setw(10) << setfill(' ') << xF << "|";
		cout << setw(10) << setfill(' ') << F << "|" << endl;
	}
	else {
		cout << "|" << setw(10) << setfill(' ') << xF << "|";
		cout << setw(10) << setfill(' ') << (int)F << "|" << endl;
	}
}


int FibNum(int n) 
{
	if (n<=1) return 1;
	return FibNum(n-1) + FibNum(n - 2);
}

void ln(double xS,double xF,double dx,double eps) {
	cout << "|" << setw(10) << setfill(' ') << "xS" << "|" << setw(20) << "F" << "|" << setw(10) << "n" << "|" << endl;
	if (!xS) {
		xS += dx;
		cout << "|" << setw(10) << setfill(' ') << 0 << "|" << setw(20) << "-" << "|" << setw(10) << 0 << "|" << endl;
	}
	for (xS; xS <= xF; xS += dx) {
		int n = 1;
		double x = 0, add = xS - 1;
		while (add > eps || add < -eps) {
			x += add;
			add = (1 - xS)*add*n / (n++ + 1);
		}
		cout << "|" << setw(10) << setfill(' ') << xS << "|" << setw(20) << x << "|" << setw(10) << n-1 << "|"<<endl;
	}
}

double kadA(double x, double y, double z)
{
	while (x != y) {
		z *= y + 1;
		y++;
	}
	return z;
}

double kadB(double x, double y, double z)
{
	if (x == y)
		return z;
	else
		return kadB(x, y + 1, (y + 1)*z);
}




int irand(int a, int b)
{
	return rand() % (b - a + 1) + a;
}