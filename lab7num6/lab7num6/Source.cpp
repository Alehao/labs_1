#include <iostream>
#include <windows.h>
#include <string>
#include <algorithm>

using namespace std;

class norms {
public:
	virtual double my_abs() = 0;
	virtual double my_norm() = 0;
};
class my_complex :public norms {
	double Re, Im;
public:
	double my_abs() override {
		return sqrt(Re*Re + Im * Im);
	}
	double my_norm() override {
		return Re * Re + Im * Im;
	}
	my_complex() :Re(0), Im(0) {}
	my_complex(double Re, double Im) :Re(Re), Im(Im) {}
};
class vector3D :public norms {
	double a, b, c;
public:
	double my_abs() override {
		return sqrt(a*a + b * b + c * c);
	}
	double my_norm() override {
		double d;
		d = max(abs(a), abs(b));
		return max(abs(c), d);
	}
	vector3D() :a(0), b(0), c(0) {}
	vector3D(double a, double b, double c) : a(a), b(b), c(c) {}
};

//void writeAbsAndNorm(norm *norm) {
//	cout << "Abs  " << norm->my_abs() << endl;
//	cout << "Norm " << norm->my_norm() << endl;
//}
double abs(norms &norm) {
	return norm.my_abs();
}
double norm(norms &norm) {
	return norm.my_norm();
}
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	my_complex complex(12, -2);
	vector3D vector(12, 0, -54);
	cout << abs(complex) << endl;
	cout << norm(complex) << endl;
	cout << abs(vector) << endl;
	cout << norm(vector) << endl;

	system("pause");
	return 0;
}